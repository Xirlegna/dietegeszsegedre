const mix = require('laravel-mix');

mix.js('resources/js/site/app.js', 'public/js/site.js');
mix.sass('resources/sass/site/app.scss', 'public/css/site.css').options({processCssUrls: false});

mix.js('resources/js/xira/app.js', 'public/js/xira.js');
mix.sass('resources/sass/xira/app.scss', 'public/css/xira.css').options({processCssUrls: false});