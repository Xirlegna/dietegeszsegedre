<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//Route::middleware('auth:api')->group(function() {});

Route::post('/contacts', 'ContactsController@store');

Route::get('/contacts/{contact}', 'ContactsController@show');

Route::get('/orders', 'OrdersController@index');
Route::post('/orders', 'OrdersController@store');
Route::get('/orders/{order}', 'OrdersController@show');
Route::patch('/orders/{order}', 'OrdersController@update');

Route::get('/order-items/{order}', 'OrderItemsController@index');
Route::post('/order-items', 'OrderItemsController@store');

Route::get('/products', 'ProductsController@index');
Route::post('/products', 'ProductsController@store'); // Site only
Route::get('/products/{product}', 'ProductsController@show'); // Admin only
Route::get('/products/admin/{product}', 'ProductsController@showAdmin');
Route::patch('/products/{product}', 'ProductsController@update');
Route::delete('/products/{product}', 'ProductsController@destroy');

Route::get('/rates', 'RatesController@index');
Route::post('/rates', 'RatesController@store');
Route::get('/rates/{rate}', 'RatesController@show');
Route::patch('/rates/{rate}', 'RatesController@update');
Route::delete('/rates/{rate}', 'RatesController@destroy');

Route::get('/notifications', 'NotificationsController@index');
