<?php

use Illuminate\Support\Facades\Route;

Route::get('/admin/{any}', 'XiraController@index')->where('any', '.*');

Route::get('/artisan', function () {
    Artisan::call('migrate');
    return 'artisan';
});

Route::get('/{any}', function () {
    return view('site');
})->where('any', '.*');