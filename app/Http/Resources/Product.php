<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    protected $admin = false;

    public function admin(bool $admin): self
    {
        $this->admin = $admin;
        return $this;
    }

    public function toArray($request): array
    {
        return [
            'data' => [
                'product_id' => $this->id,
                'name' => $this->name,
                'slug' => $this->slug,
                'price' => $this->price,
                'desc' => $this->desc,
                'created_at' => $this->getCreatedAt()
            ],
            'rate' => $this->getRate(),
            'links' => [
                'self' => $this->path()
            ]
        ];
    }

    private function getCreatedAt(): string
    {
        $created_at = new Carbon($this->created_at);
        return $created_at->isoFormat('YYYY-MM-DD HH:mm');
    }

    private function getRate()
    {
        $result = [
            'rate_num' => 0,
            'count' => 0,
            'rates' => []
        ];
        $count = [];
        $rateNum = [];

        foreach ($this->rates as $item) {
            if ($item->enabled == false && !$this->admin) {
                continue;
            }

            if ($item->enabled != false) {
                $rateNum[] = $item->rate;
            }
            $count[] = $item->rate;

            $result['rates'][] = [
                'rate_id' => $item->id,
                'rate' => $item->rate,
                'comment' => $item->comment,
                'enabled' => $item->enabled
            ];
        }

        if (count($count) === 0) {
            return $result;
        }

        $result['rate_num'] = number_format(array_sum($rateNum) / count($rateNum), 1, '.', ' ');
        $result['count'] = count($count);

        return $result;
    }
}
