<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Contact extends JsonResource
{

    public function toArray($request)
    {
        return [
            'data' => [
                'contact_id' => $this->id,
                'last_name' => $this->last_name,
                'first_name' => $this->first_name,
                'country' => $this->country,
                'zip_code' => $this->zip_code,
                'city' => $this->city,
                'street' => $this->street,
                'email' => $this->email,
                'phone' => $this->phone,
            ]
        ];
    }
}
