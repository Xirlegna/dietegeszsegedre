<?php

namespace App\Http\Resources;

use App\Contact;
use App\Product;
use App\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{

    public function toArray($request)
    {
        return [
            'data' => [
                'order_id' => $this->id,
                'contact_id' => $this->contact_id,
                'price' => $this->price,
                'viewed' => $this->viewed,
                'created_at' => $this->created_at->format('Y-m-d')

            ],
            'contact' => Contact::find($this->contact_id),
            'items' => $this->getItems(),
            'links' => [
                'self' => $this->path()
            ]
        ];
    }

    private function getItems()
    {
        $result = [];
        $items = OrderItem::where('order_id', $this->id)->get();

        foreach ($items as $val) {
            $product = Product::find($val->product_id);
            $result[] = [
                'product_id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $val->quantity
            ];
        }

        return $result;
    }
}
