<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Rate extends JsonResource
{
    public function toArray($request)
    {
        return [
            'data' => [
                'rate_id' => $this->id,
                'product_id' => $this->product_id,
                'rate' => $this->rate,
                'comment' => $this->comment,
                'enabled' => $this->enabled
            ]
        ];
    }
}
