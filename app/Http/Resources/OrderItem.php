<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItem extends JsonResource
{

    public function toArray($request)
    {
        return [
            'data' => [
                'order_item_id' => $this->id,
                'order_id' => $this->order_id,
                'product_id' => $this->product_id,
                'quantity' => $this->quantity
            ]
        ];
    }
}
