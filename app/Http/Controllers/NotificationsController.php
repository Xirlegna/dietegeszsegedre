<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{

    public function index()
    {
        $orders = Order::where('viewed', '!=', 1)->get();

        return [
            'orders' => count($orders),
            'notifications' => 0
        ];
    }
}
