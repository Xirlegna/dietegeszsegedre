<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Resources\Contact as ContactResource;
use Symfony\Component\HttpFoundation\Response;

class ContactsController extends Controller
{

    public function store()
    {
        $contact = Contact::create($this->validateData());
        return (new ContactResource($contact))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Contact $contact)
    {
        return new ContactResource($contact);
    }

    private function validateData()
    {
        return request()->validate([
            'last_name' => 'required',
            'first_name' => 'required',
            'country' => 'required',
            'zip_code' => 'required|integer',
            'city' => 'required',
            'street' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
    }
}
