<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Resources\Product as ProductResource;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends Controller
{

    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    public function store()
    {
        $sroduct = Product::create($this->validateData());
        return (new ProductResource($sroduct))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    public function showAdmin(Product $product)
    {
        return (new ProductResource($product))->admin(true);
    }

    public function update(Product $product)
    {
        $product->update($this->validateData());

        return (new ProductResource($product))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    private function validateData()
    {
        return request()->validate([
            'name' => 'required',
            'slug' => 'required',
            'price' => 'required|integer',
            'desc' => '',
        ]);
    }
}
