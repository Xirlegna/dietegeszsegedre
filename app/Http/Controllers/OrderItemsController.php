<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Http\Resources\OrderItem as OrderItemResource;
use Symfony\Component\HttpFoundation\Response;

class OrderItemsController extends Controller
{

    public function index(Order $order)
    {
        //        return OrderItemResource::collection(OrderItem::all());
        return OrderItemResource::collection(OrderItem::where('order_id', $order->id)->get());
    }

    public function store()
    {
        $orderItem = OrderItem::create($this->validateData());
        return (new OrderItemResource($orderItem))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    private function validateData()
    {
        return request()->validate([
            'order_id' => 'required|integer',
            'product_id' => 'required|integer',
            'quantity' => 'required|integer'
        ]);
    }
}
