<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Http\Resources\Rate as RateResource;
use Symfony\Component\HttpFoundation\Response;

class RatesController extends Controller
{
    public function index()
    {
        return RateResource::collection(Rate::all());
    }

    public function store()
    {
        $rate = Rate::create($this->validateData());
        return (new RateResource($rate))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Rate $rate)
    {
        return new RateResource($rate);
    }

    public function update(Rate $rate)
    {
        $rate->update($this->validateData());

        return (new RateResource($rate))->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Rate $rate)
    {
        $rate->delete();

        return response([], Response::HTTP_NO_CONTENT);
    }

    private function validateData()
    {
        return request()->validate([
            'product_id' => 'required|integer',
            'rate' => 'required|integer',
            'comment' => '',
            'enabled' => ''
        ]);
    }
}
