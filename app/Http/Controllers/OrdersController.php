<?php

namespace App\Http\Controllers;

use App\Order;
use App\Http\Resources\Order as OrderResource;
use Symfony\Component\HttpFoundation\Response;

class OrdersController extends Controller
{

    public function index()
    {
        return OrderResource::collection(Order::all());
    }

    public function store()
    {
        $order = Order::create($this->validateData());
        return (new OrderResource($order))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    public function update(Order $order)
    {
        $order->update($this->validateData());
    }

    private function validateData()
    {
        return request()->validate([
            'contact_id' => 'required|integer',
            'price' => 'required|integer',
            'viewed' => ''
        ]);
    }
}
