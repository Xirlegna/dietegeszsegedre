<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
