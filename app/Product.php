<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at'];

    public function path()
    {
        return '/admin/products/' . $this->id;
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }
}
