<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $guarded = [];
    protected $dates = ['created_at'];

    public function path()
    {
        return '/admin/orders/' . $this->id;
    }

    public function setCreatedAtAttribute($createdAt)
    {
        $this->attributes['created_at'] = Carbon::parse($createdAt);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
