import Vue from 'vue';
import VueRouter from 'vue-router';

import ContactsIndex from './views/ContactsIndex';
import NotificationsIndex from './views/NotificationsIndex';
import OrdersIndex from './views/OrdersIndex';
import OrdersShow from './views/OrdersShow';
import ProductsCreate from './views/ProductsCreate';
import ProductsIndex from './views/ProductsIndex';
import ProductsShow from './views/ProductsShow';
import XiraContactsIndex from './views/XiraContactsIndex';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/admin/contacts',
            component: ContactsIndex,
            name: 'contactsIndex',
            meta: {title: 'Felhasználók'}
        },
        {
            path: '/admin/notifications',
            component: NotificationsIndex,
            name: 'notificationsIndex',
            meta: {title: 'Értesítések'}
        },
        {
            path: '/admin/orders',
            component: OrdersIndex,
            name: 'ordersIndex',
            meta: {title: 'Rendelések'}
        },
        {
            path: '/admin/orders/:id',
            component: OrdersShow,
            name: 'ordersShow',
            meta: {title: 'Rendelés'}
        },
        {
            path: '/admin/products/create',
            component: ProductsCreate,
            name: 'productsCreate',
            meta: {title: 'Termék készítés'}
        },
        {
            path: '/admin/products',
            component: ProductsIndex,
            name: 'productsIndex',
            meta: {title: 'Termékek'}
        },
        {
            path: '/admin/products/:id',
            component: ProductsShow,
            name: 'productsShow',
            meta: {title: 'Termék'}
        },
        {
            path: '/admin/xira-contacts',
            component: XiraContactsIndex,
            name: 'XiraContactsIndex',
            meta: {title: 'Xira felhasználól'}
        }
    ],
    mode: 'history'
});