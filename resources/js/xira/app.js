import Vue from 'vue';
import router from './router';
import { store } from './store/store';
import App from './components/App';

Vue.prototype.$slug = (str) => {
    str = str.replace(/^\s+|\s+$/g, '');
    str = str.toLowerCase();

    let from = "àáäâèéëêìíïîòóöőôùúüűûñç·/_,:;";
    let to = "aaaaeeeeiiiiooooouuuuunc------";
    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-');

    return str;
};

require('./bootstrap');

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router,
    store
});