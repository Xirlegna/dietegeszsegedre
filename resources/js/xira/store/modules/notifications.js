const state = {
    orders: 0,
    notifications: 0
};

const getters = {
    orders: state => {
        return state.orders;
    },
    notifications: state => {
        return state.notifications;
    }
};

const mutations = {
    setOrders: (state, orders) => {
        state.orders = orders;
    },
    decOrders: (state) => {
        state.orders -= 1;
    },
    setNotifications: (state, notifications) => {
        state.notifications = notifications;
    },
    decNotifications: (state) => {
        state.notifications -= 1;
    }
};

const actions = {
    setOrders: ({commit}, orders) => {
        this.$store.dispatch('setOrders', orders);
    },
    decOrders: ({commit}) => {
        this.$store.dispatch('decOrders');
    },
    setNotifications: ({commit}, notifications) => {
        this.$store.dispatch('setNotifications', notifications);
    },
    decNotifications: ({commit}) => {
        this.$store.dispatch('decNotifications');
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};