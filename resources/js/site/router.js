import Vue from 'vue';
import VueRouter from 'vue-router';

import BasketPage from './views/BasketPage';
import LandingPage from './views/LandingPage';
import OrderFinish from './views/OrderFinish';
import Webshop from './views/Webshop';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            component: LandingPage,
            name: 'landingPage',
            meta: {title: 'Dietegészségedre'}
        },
        {
            path: '/bolt',
            component: Webshop,
            name: 'webshop',
            meta: {title: 'Termékek'}
        },
        {
            path: '/kosar',
            component: BasketPage,
            name: 'basketPage',
            meta: {title: 'Kosár'}
        },
        {
            path: '/rendeles-vege',
            component: OrderFinish,
            name: 'orderFinish',
            meta: {title: 'Rendelés vége'}
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0};
    },
    mode: 'history'
});