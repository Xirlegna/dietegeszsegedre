import Vue from 'vue';
import VueCookies from 'vue-cookies';
import router from './router';
import { store } from './store/store';
import App from './components/App';

require('./bootstrap');

Vue.use(VueCookies);

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router,
    store
});