const state = {
    rate: null
};

const getters = {
    rate: state => {
        return state.rate;
    },
};

const mutations = {
    setRate: (state, rate) => {
        state.rate = rate;
    }
};

const actions = {
    setRate: ({commit}, rate) => {
        this.$store.dispatch('setRate', rate);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}