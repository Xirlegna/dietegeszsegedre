const state = {
    price: 0,
    products: []
};

const getters = {
    basketPrice: state => {
        return state.price;
    },
    basketProducts: state => {
        return state.products;
    }
};

const mutations = {
    setBasketFromCookie: (state) => {
        const items = JSON.parse(window.$cookies.get('basket_items'));
        for (const i in items) {
            const item = items[i];

            state.products.push({
                id: item.id,
                name: item.name,
                quantity: Math.round(item.quantity),
                price: item.price
            });
            
            state.price += item.price * item.quantity;
        }
    },
    putBasket: (state, order) => {
        const record = state.products.find(element => element.id === order.id);

        if (record) {
            record.quantity += Math.round(order.quantity);
        } else {
            state.products.push({
                id: order.id,
                name: order.name,
                quantity: Math.round(order.quantity),
                price: order.price
            });
        }

        window.$cookies.set('basket_items', JSON.stringify(state.products));

        state.price += order.price * order.quantity;
    },
    setQuantity: (state, order) => {
        const record = state.products.find(element => element.id === order.id);
        const index = state.products.findIndex(element => element.id === order.id);

        if (record) {
            if (Math.round(order.quantity) === 0) {
                state.products.splice(index, 1);
            }

            const oldPrice = record.price * record.quantity;
            const newPrice = record.price * Math.round(order.quantity);

            if (oldPrice > newPrice) {
                state.price -= Math.abs(oldPrice - newPrice);
            } else {
                state.price += Math.abs(oldPrice - newPrice);
            }

            record.quantity = Math.round(order.quantity);
            
            window.$cookies.set('basket_items', JSON.stringify(state.products));
        }
    },
    clearBasket: (state) => {
        window.$cookies.remove('basket_items');
        state.price = 0;
        state.products = [];
    },
    saveBasket: () => {
        
    }
};

const actions = {
    setBasketFromCookie: ({commit}) => {
        this.$store.dispatch('setBasketFromCookie');
    },
    putBasket: ({commit}, order) => {
        this.$store.dispatch('putBasket', order);
    },
    setQuantity: ({commit}, order) => {
        this.$store.dispatch('setQuantity', order);
    },
    clearBasket: ({commit}) => {
        this.$store.dispatch('clearBasket');
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}