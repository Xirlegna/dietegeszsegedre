const state = {
    contact: {}
};

const getters = {
    contact: state => {
        return state.contact;
    }
};

const mutations = {
    setContact: (state, contact) => {
        state.contact = contact;
    }
};

const actions = {
    setContact: ({commit}, contact) => {
        this.$store.dispatch('setContact', contact);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};