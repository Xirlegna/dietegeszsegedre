import Vue from 'vue';
import Vuex from 'vuex';
import contact from './modules/contact';
import products from './modules/products';
import rate from './modules/rate';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        contact,
        products,
        rate
    }
});