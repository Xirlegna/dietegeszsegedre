<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration {

    public function up() {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('last_name');
            $table->string('first_name');
            $table->string('country');
            $table->integer('zip_code');
            $table->string('city');
            $table->string('street');
            $table->string('email');
            $table->string('phone');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('contacts');
    }

}
