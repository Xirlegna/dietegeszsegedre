<?php

use App\Rate;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    return [
        'product_id' => 2,
        'rate' => $faker->numberBetween(1, 5),
        'comment' => 'comment',
        'enabled' => false,
    ];
});
