<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'price' => $faker->numberBetween(999, 19999),
        'desc' => $faker->realText(),
    ];
});
