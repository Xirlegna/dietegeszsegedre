<?php

use App\Contact;
use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'contact_id' => factory(Contact::class),
        'price' => 10000,
        'viewed' => '2020-08-03',
    ];
});
