<?php

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'last_name' => $faker->firstName,
        'first_name' => $faker->lastName,
        'country' => $faker->country,
        'zip_code' => $faker->numberBetween(1000, 9999),
        'city' => $faker->city,
        'street' => $faker->streetName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ];
});
