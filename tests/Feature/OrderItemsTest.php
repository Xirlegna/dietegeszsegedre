<?php

namespace Tests\Feature;

use App\OrderItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class OrderItemsTest extends TestCase
{

    use RefreshDatabase;

    public function testAnOrderItemCanBeAdded()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/order-items', $this->data());

        $orderItem = OrderItem::first();

        $this->assertEquals(1, $orderItem->order_id);
        $this->assertEquals(1, $orderItem->product_id);
        $this->assertEquals(1, $orderItem->quantity);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    private function data()
    {
        return [
            'order_id' => 1,
            'product_id' => 1,
            'quantity' => 1
        ];
    }
}
