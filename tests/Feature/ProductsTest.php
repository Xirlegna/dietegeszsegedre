<?php

namespace Tests\Feature;

use App\Rate;
use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductsTest extends TestCase
{

    use RefreshDatabase;

    public function testProductFieldsAreRequired()
    {
        collect(['name', 'slug'])->each(function ($field) {
            $response = $this->post('/api/products', array_merge($this->data(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Product::all());
        });
    }

    public function testProductFieldsAreInteger()
    {
        collect(['price'])->each(function ($field) {
            $response = $this->post('/api/products', array_merge($this->data(), [$field => 'abc']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Product::all());
        });
    }

    public function testCanListAllProducts()
    {
        factory(Product::class)->create();
        factory(Product::class)->create();

        $response = $this->get('/api/products');
        $this->assertEquals(2, count($response->getData()->data));
    }

    public function testAProductWithAListOfRatesCanBeFetched()
    {
        $product = Product::create($this->data());

        $product->rates()->save(
            Rate::create([
                'product_id' => 0,
                'rate' => 1,
                'comment' => 'Obscene product',
                'enabled' => false
            ])
        );

        $product->rates()->save(
            Rate::create([
                'product_id' => 0,
                'rate' => 5,
                'comment' => 'Good product',
                'enabled' => true
            ])
        );

        $product->rates()->save(
            Rate::create([
                'product_id' => 0,
                'rate' => 1,
                'comment' => 'Bad product',
                'enabled' => true
            ])
        );

        $response = $this->get('/api/products/' . $product->id);

        $response->assertJsonCount(3)->assertJson([
            'data' => [
                'product_id' => $product->id,
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price,
                'desc' => $product->desc
            ],
            'links' => [
                'self' => $product->path()
            ],
            'rate' => [
                'rate_num' => 3,
                'count' => 2,
                'rates' => [
                    0 => [
                        'rate' => 5,
                        'comment' => 'Good product'
                    ],
                    1 => [
                        'rate' => 1,
                        'comment' => 'Bad product'
                    ]
                ]
            ]
        ]);
    }

    public function testAPruductCanBeAdded()
    {
        $response = $this->post('/api/products', $this->data());

        $product = Product::first();

        $this->assertEquals('Test Product Name', $product->name);
        $this->assertEquals('test-product-name', $product->slug);
        $this->assertEquals(10000, $product->price);
        $this->assertEquals('Test Product Description', $product->desc);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(['data' => ['product_id' => $product->id], 'links' => ['self' => $product->path()]]);
    }

    public function testAProductCanBePatched()
    {
        $product = factory(Product::class)->create();

        $response = $this->patch('/api/products/' . $product->id, $this->data());

        $product = $product->fresh();

        $this->assertEquals('Test Product Name', $product->name);
        $this->assertEquals('test-product-name', $product->slug);
        $this->assertEquals(10000, $product->price);
        $this->assertEquals('Test Product Description', $product->desc);
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testAProductCanBeDelete()
    {
        $product = factory(Product::class)->create();

        $response = $this->delete('/api/products/' . $product->id);

        $this->assertCount(0, Product::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function data()
    {
        return [
            'name' => 'Test Product Name',
            'slug' => 'test-product-name',
            'price' => 10000,
            'desc' => 'Test Product Description',
        ];
    }
}
