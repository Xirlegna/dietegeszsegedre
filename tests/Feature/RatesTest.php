<?php

namespace Tests\Feature;

use App\Rate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class RatesTest extends TestCase
{
    use RefreshDatabase;

    public function testRateFieldsAreRequired()
    {
        collect(['product_id', 'rate'])->each(function ($field) {
            $response = $this->post('/api/rates', array_merge($this->data(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Rate::all());
        });
    }

    public function testProductFieldsAreInteger()
    {
        collect(['product_id', 'rate'])->each(function ($field) {
            $response = $this->post('/api/rates', array_merge($this->data(), [$field => 'abc']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Rate::all());
        });
    }

    public function testAListOfRatesCanBeFetched()
    {
        factory(Rate::class)->create();
        factory(Rate::class)->create();

        $response = $this->get('/api/rates');
        $this->assertEquals(2, count($response->getData()->data));
    }

    public function testARateCanBeRetrieved()
    {
        $rate = factory(Rate::class)->create();

        $response = $this->get('/api/rates/' . $rate->id);

        $response->assertJson([
            'data'  => [
                'product_id' => $rate->product_id,
                'rate' => $rate->rate,
                'comment' => $rate->comment,
                'enabled' => $rate->enabled
            ]
        ]);
    }

    public function testARateCanBeAdded()
    {
        $response = $this->post('/api/rates', $this->data());

        $rate = Rate::first();

        $this->assertEquals(1, $rate->product_id);
        $this->assertEquals(5, $rate->rate);
        $this->assertEquals('Good product', $rate->comment);
        $this->assertEquals(0, $rate->enabled);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testAFileCanBePatched()
    {
        $rate = factory(Rate::class)->create();

        $response = $this->patch('/api/rates/' . $rate->id, $this->data());

        $rate = $rate->fresh();

        $this->assertEquals(1, $rate->product_id);
        $this->assertEquals(5, $rate->rate);
        $this->assertEquals('Good product', $rate->comment);
        $this->assertEquals(0, $rate->enabled);
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testAFileCanBeDeleted()
    {
        $rate = factory(Rate::class)->create();

        $response = $this->delete('/api/rates/' . $rate->id);

        $this->assertCount(0, Rate::all());
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function data()
    {
        return [
            'product_id' => 1,
            'rate' => 5,
            'comment' => 'Good product',
            'enabled' => false
        ];
    }
}
