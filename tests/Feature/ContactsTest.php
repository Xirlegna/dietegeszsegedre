<?php

namespace Tests\Feature;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ContactsTest extends TestCase
{
    use RefreshDatabase;

    public function testContactFieldsAreRequired()
    {
        collect(['last_name', 'first_name', 'country', 'zip_code', 'city', 'street', 'email', 'phone'])->each(function ($field) {
            $response = $this->post('/api/contacts', array_merge($this->data(), [$field => '']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Contact::all());
        });
    }

    public function testContactFieldsAreInteger()
    {
        collect(['zip_code'])->each(function ($field) {
            $response = $this->post('/api/contacts', array_merge($this->data(), [$field => 'abc']));

            $response->assertSessionHasErrors($field);
            $this->assertCount(0, Contact::all());
        });
    }

    public function testAContactCanBeAdded()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/contacts', $this->data());

        $contact = Contact::first();

        $this->assertEquals('Teszt', $contact->last_name);
        $this->assertEquals('Norbert', $contact->first_name);
        $this->assertEquals('Magyarország', $contact->country);
        $this->assertEquals(6200, $contact->zip_code);
        $this->assertEquals('Kiskőrös', $contact->city);
        $this->assertEquals('Korcin u, 99', $contact->street);
        $this->assertEquals('test.norbert@gmail.com', $contact->email);
        $this->assertEquals('+36(30)9999999', $contact->phone);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    private function data()
    {
        return [
            'last_name' => 'Teszt',
            'first_name' => 'Norbert',
            'country' => 'Magyarország',
            'zip_code' => 6200,
            'city' => 'Kiskőrös',
            'street' => 'Korcin u, 99',
            'email' => 'test.norbert@gmail.com',
            'phone' => '+36(30)9999999',
        ];
    }
}
