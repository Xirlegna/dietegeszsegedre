<?php

namespace Tests\Feature;

use App\Order;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class OrdersTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function testCanListAllOrders()
    {
        factory(Order::class)->create();
        factory(Order::class)->create();

        $response = $this->get('/api/orders/');

        $this->assertCount(2, Order::all());
    }

    /** @test */
    public function testAnOrderCanBeAdded()
    {
        $response = $this->post('/api/orders', $this->data());

        $order = Order::first();

        $this->assertEquals(1, $order->contact_id);
        $this->assertEquals(10000, $order->price);
        $this->assertEquals(1, $order->viewed);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(['data' => ['order_id' => $order->id], 'links' => ['self' => $order->path()]]);
    }

    private function data()
    {
        return [
            'contact_id' => 1,
            'price' => 10000,
            'viewed' => 1
        ];
    }
}
